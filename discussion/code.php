<?php 

//Access Modifiers and Encapsulation
// Protect object properties by using access modifiers
// Change the value of object prop using encapsulation via getter and setter


// Access Modifiers
// In OOP access modifiers exist to control the level of access a code has using objects properties or methods (control access in properties of an object)
// Protect code from unintentional changes

// Encapsulation
// Also provides protection particularly in object properties (ex. acess the property but can't change it or access username but not password property)
// Possible via getter and setter methods

// [SECTION] Access modifier PUBLIC
class BuildingA {
	public $name;
	public $floors;
	public $address;

	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;


	}
}

$buildingA = new BuildingA('Trial Building A', 10, 'Manila City, Manila');

class CondominiumA extends BuildingA{

}

$condominiumA = new CondominiumA('Trial Condominium A', 20, 'Quezon City, Manila');

// With the public modifier the properties can be access directly and even modify it.
// Another thing that with public modifier the properties of BuildingA class is inherited by CondominiumA class




// [SECTION] Access modifier PRIVATE

class BuildingB {
	private $name;
	private $floors;
	private $address;

	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	// Since we cannot access private properties we can only use a GETTER FUNCTION
	public function getName(){
		return $this->name;
	}
	
	// Since we cannot modify private propeties we can only use SETTER FUNCTION 
	public function setName($name){
		$this->name = $name;
	}

}

class CondominiumB extends BuildingB{

}


$buildingB = new BuildingB('Trial Building B', 20, 'Manila City, Manila');

$condominiumB = new CondominiumB('Trial Condominium B', 40, 'Quezon City, Manila');


// With the private modifier the properties CANNOT BE accessed directly and but can modify it directly.
// Another thing that with private modifier the properties of BuildingB class is not inherited by CondominiumB class



// [SECTION] Access modifier PROTECTED

class BuildingC {
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	// Since we cannot access protected properties we can only use a GETTER FUNCTION
	public function getName(){
		return $this->name;
	}
	
	// Since we cannot modify protected propeties we can only use SETTER FUNCTION 
	public function setName($name){
		$this->name = $name;
	}

}

class CondominiumC extends BuildingC{

}


$buildingC = new BuildingC('Trial Building C', 40, 'Manila City, Manila');

$condominiumC = new CondominiumC('Trial Condominium C', 80, 'Quezon City, Manila');


// With the protected modifier the properties CANNOT BE accessed directly and modify it directly.
// Another thing that with protected modifier the properties of BuildingC class is not inherited by CondominiumC class





// Protected properties another example
class Drink {
	protected $name;

	public function __construct($name){
		$this->name = $name;
	}

	// getter for the name prop
	public function getName() {
		return $this->name;
	}

	// setter for the name property
	public function setName($name) {
		$this->name = $name;
	}
}


$milk = new Drink('Alaska');

class Coffee extends Drink {

}

$kopiko = new Coffee('Kopiko');




// Summary

// Access modifier (public)		
// Retrieval (yes)	
// Modification (yes)	
// Inheritance (yes)	
// Retrival of Subclass (yes)	
// Modification of Subclass (yes)

// Access modifier (private)	
// Retrieval (no) - use getter
// Modification (no) - use setter and then getter for update
// Inheritance (no)	
//Retrival of Subclass (no)	- use getter
// Modification of Subclass (yes) - SUPRISINGLY(new property); You can still use setter but for update to show use getter

// Access modifier (protected)	
// Retrieval (no)  - use getter
//Modification (no)	- use setter and then getter for update
//Inheritance (yes)	
//Retrival of Subclass (no)	- use getter
//Modification of Subclass (no) - use setter and then getter for update


?>