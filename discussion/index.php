<?php require_once "./code.php"?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S04 : Access Modifiers and Encapsulation</title>

	</head>
	<body>

		<h1>Access Modifiers</h1>

		<!-- PUBLLIC -->

		<h3>Building A</h3>
		<!-- with public it can be accessed and even modified-->


		<p><?php var_dump($buildingA)?></p>
		<p><?php echo $buildingA->name;?></p>
		<p><?php $buildingA->name = 'Changed Trial Building A';?></p>
		<p>Changes applied: <?php echo $buildingA->name;?></p>


		<h3>Condominium A</h3>

		<!-- with public it can be inherited, accessed and even modified-->
		<p><?php var_dump($condominiumA)?></p>
		<p><?php echo $condominiumA->name;?></p>
		<p><?php $condominiumA->name = 'Changed Trial Condominium A';?></p>
		<p>Changes applied: <?php echo $condominiumA->name;?></p>


		<!-- PRIVATE -->

		<h3>Building B</h3>

		<!-- with private as default it CANNOT BE accessed (use GETTER FUNCTION TO ACCESS) and even modified (use SETTER FUNCTION TO MODIFY)-->


		<!-- print_r(), var_dump() and var_export() will show protected and private properties of objects -->
		<!-- "BuildingB":private -->
		<p><?php var_dump($buildingB)?></p>

		<!--  Uncaught Error: Cannot access private property BuildingB::$name  -->
		<p><?php //echo $buildingB->name;?></p>

		<!-- With the above concept the only way we can access it is GETTER FUNCTION -->
		<p><?php echo $buildingB->getName();?></p>


		<!--  Uncaught Error: Cannot access private property BuildingB::$name  -->
		<!-- Changes didn't apply -->
		<p><?php //$buildingB->name = 'Changed Trial Building B';?></p>
		<!--  Uncaught Error: Cannot access private property BuildingB::$name  -->
		<p>Changes applied: <?php //echo $buildingB->name;?></p>

		<!-- With the above concept only way we can modify it is SETTER FUNCTION -->
		<p><?php $buildingB->setName('Changed Trial Building B');?></p>
		<!-- Remember with the private conpect the only way we can access it is GETTER FUNCTION -->
		<p>Changes applied: <?php echo $buildingB->getName();;?></p>



		<h3>Condominium B</h3>

		<!-- with private it CANNOT BE inherited and accessed (use GETTER FUNCTION TO ACCESS) BUT Surprisingly in can be modified (see explaination)-->



		<!-- print_r(), var_dump() and var_export() will show protected and private properties of objects -->
		<!-- "BuildingB":private  means can't be inherited-->
		<p><?php var_dump($condominiumB)?></p>


		<!-- Private properties are not inherited by subclasses -->
		<!-- In PHP, private properties are not directly accessible from outside the class (retrival), including subclasses(inheritance). -->
		<!-- Undefined property: CondominiumB::$name since it can't be inherited -->
		<p><?php //echo $condominiumB->name;?></p>

		<!-- With the above concept the only way we can access it is GETTER FUNCTION -->
		<!-- Inherited from the PARENT since its in public but its still good as with this we can control the output -->
		<p>Uses getter :<?php echo $condominiumB->getName();?></p>


		<!-- Surprisingly even though it can't be inherited but you can modify it and eventually access the modified -->

		<!-- 

		---Modifying Private Property:

		PHP allows you to modify the private property from outside the class, even though direct access is not allowed.
		
		This behavior can seem unexpected, but it's due to how PHP handles property visibility and access control.
		
		When you try to modify a private property from outside the class, PHP doesn't prevent it. Instead, it treats the modification as creating a new property with the same name within the object.
		
		So, the code $condominiumB->name = 'Changed Trial Condominium A'; creates a new property $name in the CondominiumB object and assigns the value 'Changed Trial Condominium A' to it.
		
		This new property is entirely independent of the $name property defined in the BuildingB class, and the two properties are not related.
		
		---Accessing Modified Property:

		After modifying the private property using $condominiumB->name = 'Changed Trial Condominium A';, you can access the newly created property in the CondominiumB object, but it doesn't have any connection with the private property $name defined in the BuildingB class.
		
		When you use echo $condominiumB->name;, it refers to the newly created property, not the original private property $name in the BuildingB class.

		 -->

		 <!-- Summary : This creates new property called $name independent of the private property $name defined in the BuildingB class. -->
		<p><?php $condominiumB->name = 'Changed Trial Condominium B';?></p>
		<p>Changes applied: <?php echo $condominiumB->name;?></p>


		<!-- You can also use SETTER FUNCTION to modify -->
		<p><?php $condominiumB->setName('SetChanged Trial Building B');?></p>

		<!-- This code will work but will return the direct modification from above since it will refer the newly created $name independent of private property that is not inherited-->
		<p>SetChanges applied: <?php echo $condominiumB->name;?></p>

		<!-- To get the modification of setName you need to use the setter function as its the one inherited -->
		<p>SetChanges applied: <?php echo $condominiumB->getName();?></p>





		<h3>Building C</h3>
		<!-- with protected it CANNOT BE accessed (use GETTER FUNCTION TO ACCESS) and even modified (use SETTER FUNCTION TO MODIFY-->

		<!-- print_r(), var_dump() and var_export() will show protected and private properties of objects -->
		<!-- "name":protected  means can't be accessed-->
		<p><?php var_dump($buildingC)?></p>

		<!-- Uncaught Error: Cannot access protected property BuildingC::$name -->
		<p><?php //echo $buildingC->name;?></p>

		<!-- With the above concept the only way we can access it is GETTER FUNCTION -->
		<p><?php echo $buildingC->getName();?></p>

		<!--  Uncaught Error: Cannot access protected property BuildingC::$name  -->
		<!-- Changes didn't apply -->
		<p><?php //$buildingC->name = 'Changed Trial Building A';?></p>
		<!-- Uncaught Error: Cannot access protected property BuildingC::$name -->
		<p>Changes applied: <?php //echo $buildingC->name;?></p>


		<!-- With the above concept only way we can modify it is SETTER FUNCTION -->
		<p><?php $buildingC->setName('Changed Trial Building C');?></p>
		<!-- Remember with the protected conpect the only way we can access it is GETTER FUNCTION -->
		<p>Changes applied: <?php echo $buildingC->getName();;?></p>


		<h3>Condominium C</h3>

		<!-- with protected it can be inherited-->

		<!-- print_r(), var_dump() and var_export() will show protected and private properties of objects -->
		<p><?php var_dump($condominiumC)?></p>

		<!-- Uncaught Error: Cannot access protected property CondominiumC::$name -->
		<p><?php //echo $condominiumC->name;?></p>

		<!-- With the above concept the only way we can access it is GETTER FUNCTION -->
		<p><?php echo $condominiumC->getName();?></p>

		<!--  Uncaught Error: Cannot access protected property CondominiumC::$name -->
		<!-- Changes didn't apply -->
		<p><?php //$condominiumC->name = 'Changed Trial Condominium A';?></p>
		<!-- Uncaught Error: Cannot access protected property CondominiumC::$name -->
		<p>Changes applied: <?php //echo $condominiumC->name;?></p>

		<!-- With the above concept only way we can modify it is SETTER FUNCTION -->
		<p><?php $condominiumC->setName('Changed Trial Condominium C');?></p>
		<!-- Remember with the protected conpect the only way we can access it is GETTER FUNCTION -->
		<p>Changes applied: <?php echo $condominiumC->getName();;?></p>




	</body>
</html>