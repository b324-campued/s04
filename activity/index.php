<?php require_once "./code.php"?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S04 : Access Modifiers and Encapsulation</title>

	</head>
	<body>

		<h1>Building</h1>
		<p> The name of the building is <?php echo $building->getName()?> . </p>
		<p> The <?php echo $building->getName()?> has <?php echo $building->getFloors()?> floors. </p>
		<p> The <?php echo $building->getName()?> is located at <?php echo $building->getAddress()?> . </p>
		<p><?php  $building->setAddress('Caswynn Complex');?></p>
		<p> The name of building has been changed to <?php echo $building->getAddress()?> . </p>


		<h1>Condominium</h1>
		<p> The name of the condominium is <?php echo $condominium->getName()?> . </p>
		<p> The <?php echo $condominium->getName()?> has <?php echo $condominium->getFloors()?> floors. </p>
		<p> The <?php echo $condominium->getName()?> is located at <?php echo $condominium->getAddress()?> . </p>
		<p><?php  $condominium->setAddress('Enzo Tower');?></p>
		<p> The <?php echo $condominium->getName()?> is located at <?php echo $condominium->getAddress()?> . </p>




	</body>
</html>