<?php 

class Building {
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	// Getter functions
	public function getName(){
		return $this->name;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function getAddress(){
		return $this->address;
	}

	// Setter function for address only

	public function setAddress($address){
		$this->address = $address;
	}
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Phippines');

class Condominium extends Building{

}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Manila');


?>